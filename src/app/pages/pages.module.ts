import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PagesRoutingModule} from './pages-routing.module';
import {SlickCarouselModule} from 'ngx-slick-carousel';

import {AboutUsComponent} from './about-us/about-us.component';
import {ErrorPageComponent} from './error-page/error-page.component';
import {LoginComponent} from './login/login.component';
import {RegisterComponent} from './register/register.component';
import {CollectionComponent} from './collection/collection.component';
import {ForgetPasswordComponent} from './forget-password/forget-password.component';
import {ContactComponent} from './contact/contact.component';
import {DashboardComponent} from './dashboard/dashboard.component';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {HeaderClienteComponent} from './cliente/header-cliente/header-cliente.component';
import {SidebarComponent} from './cliente/sidebar/sidebar.component';

@NgModule({
  imports: [
    CommonModule,
    PagesRoutingModule,
    SlickCarouselModule,
    FormsModule,
    ReactiveFormsModule
  ],
  declarations: [
    AboutUsComponent,
    ErrorPageComponent,
    LoginComponent,
    RegisterComponent,
    CollectionComponent,
    ForgetPasswordComponent,
    ContactComponent,
    DashboardComponent,
    HeaderClienteComponent,
    SidebarComponent
  ]
})
export class PagesModule {
}
