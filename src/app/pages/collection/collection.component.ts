import {Component, OnInit} from '@angular/core';
import {ProductsService} from '../../staff/services/products/products.service';
import {Collection} from '../../staff/interfaces/products/collection';
import {environment} from '../../../environments/environment';

@Component({
  selector: 'app-collection',
  templateUrl: './collection.component.html',
  styleUrls: ['./collection.component.scss']
})
export class CollectionComponent implements OnInit {

  public colleciones: Collection[];

  constructor(private ProductService: ProductsService) {
    this.ProductService.getCollection().subscribe(colleciones => {
      this.colleciones = colleciones;
      this.colleciones.forEach(e => {
        e.img = environment.urlImage + e.img;
      });
    });
  }

  ngOnInit() {
  }

}
