import {Component, OnInit} from '@angular/core';
import {ProductsService} from '../../../shared/services/products.service';
import {Router} from '@angular/router';
import {LoginService} from "../../../shared/services/login.service";
import * as $ from 'jquery';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor(private ProductService: ProductsService, private router: Router, private LoginService: LoginService) {
  }

  ngOnInit() {
  }

  public logout() {
    localStorage.removeItem(btoa('auth'));
    this.LoginService.setData();
    this.ProductService.currency = 'AUD';
    this.hideMenu();
    this.router.navigate(['/paginas/login']);
  }
  public showMenu(){
    $('.dashboard-left').css('left', '-15px');
  }
  public hideMenu(){
    $('.dashboard-left').css('left', '-315px');
  }
}
