import {Component, OnInit} from '@angular/core';
import {Login} from '../../../shared/classes/login/login';

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.scss']
})
export class InicioComponent implements OnInit {
  public data: Login;

  constructor() {
    this.data = atob(localStorage.getItem(btoa('auth')))!==null ? JSON.parse(atob(localStorage.getItem(btoa('auth')))):{};
  }

  ngOnInit() {
  }

}
