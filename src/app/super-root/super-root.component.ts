import {Component, OnInit} from '@angular/core';
import {NavigationEnd, Router} from '@angular/router';

declare var $: any;

@Component({
  selector: 'app-super-root',
  templateUrl: './super-root.component.html',
  styleUrls: ['./super-root.component.scss']
})
export class SuperRootComponent implements OnInit {
  public url: any;

  constructor(private router: Router) {
    this.router.events.subscribe((event) => {
      if (event instanceof NavigationEnd) {
        this.url = event.url;
      }
    });
  }

  ngOnInit() {
    $.getScript('assets/js/script.js');
  }

}
