import {Injectable} from '@angular/core';
import {HttpClient, HttpResponse} from '@angular/common/http';
import {environment} from '../../../../environments/environment';
import {map} from 'rxjs/operators';
import {Observable} from 'rxjs';
import {Pedidos} from '../../interfaces/pedidos/pedidos';
import {Products} from '../../interfaces/pedidos/products';

@Injectable({
  providedIn: 'root'
})
export class PedidosService {
  // public orders: Pedidos[];

  constructor(private http: HttpClient) {
  }

  public Orders(): Observable<Pedidos[]> {
    return this.http.get<Pedidos[]>(`${environment.url}orders`, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<Pedidos[]>) => {
      response.body.forEach(e => {
        e.cantidad = 0;
        e.products.forEach(ele => {
          e.cantidad += parseInt(String(ele.quantity));
        });
      });
      return response.body;
    }));
  }

  public getOrders(): Observable<Pedidos[]> {
    return this.Orders();
  }

  public getOrder(id): Observable<Pedidos> {
    return this.Orders().pipe(map(items => {
      return items.find((item: Pedidos) => {
        return String(item.id) === String(id);
      });
    }));
  }

  public getTotalAmount(mult: Products[]): number {
    let total = 0;
    let quantity = this.getTotalQuantity(mult) < 7 ? 0.75 : 0.65;
    let shipping = this.getTotalQuantity(mult) < 4 ? 6000 : 0;
    mult.forEach(e => {
      total += e.price * e.quantity;
    });
    return (total * quantity) + shipping;
  }

  public getTotalQuantity(mult: Products[]): number {
    let total = 0;
    mult.forEach(e => {
      total += e.quantity;
    });
    return total;
  }

  public changeStatus(id: number, status: string): Observable<boolean> {
    return this.http.put<boolean>(`${environment.url}changeStatus?id=${id}&status=${status}`, {}, {
      headers: {'Content-Type': 'application/json'},
      observe: 'response'
    }).pipe(map((response: HttpResponse<boolean>) => response.body));
  }
}
