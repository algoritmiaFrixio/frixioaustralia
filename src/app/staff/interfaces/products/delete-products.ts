import {Products} from "./products";

export interface DeleteProducts {
  message: string;
  products: Products[]
}
