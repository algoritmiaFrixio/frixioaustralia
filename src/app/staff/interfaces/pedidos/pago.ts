export interface Pago {
  accountId?: string;
  amount?: number;
  buyerEmail?: string;
  buyerFullName?: string;
  confirmationUrl?: string;
  currency?: string;
  description?: string;
  id?: number;
  merchantId?: string;
  postalcode?: string;
  responseUrl?: string;
  shippingAddress?: string;
  shippingCity?: string;
  shippingCountry?: string;
  shippingValue?: number;
  state?: string;
  tax?: number;
  taxReturnBase?: number;
  telephone?: string;
  test?: string;
}
