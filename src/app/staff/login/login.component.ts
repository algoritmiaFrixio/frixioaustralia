import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {LoginService} from '../services/login/login.service';
import {Login} from '../interfaces/login/login';
import {Router} from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  formLogin: FormGroup;
  error = false;
  message = '';

  constructor(private Loginservice: LoginService, private route: Router) {
    this.formLogin = new FormGroup({
      usuario: new FormControl('', Validators.required),
      contrasena: new FormControl('', Validators.required),
    });
  }

  ngOnInit() {
    if (sessionStorage.getItem('user') !== null) {
      this.route.navigate(['admin/dashboard']);
    }
  }

  public auth(form: FormGroup) {
    this.Loginservice.auth(form.value).subscribe((response: Login) => {
      sessionStorage.setItem('user', JSON.stringify(response));
      this.route.navigate(['admin/dashboard']);
    }, error1 => {
      this.message = error1.error.message;
      this.error = true;
      setTimeout(() => {
        this.error = false;
      }, 3000);
    });
  }
}
