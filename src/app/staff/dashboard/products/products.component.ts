import {Component, OnInit} from '@angular/core';
import {FormControl, FormGroup, Validators} from '@angular/forms';
import {ProductsService} from '../../services/products/products.service';
import {Categories} from '../../../shared/classes/categories';
import {Sizes} from '../../interfaces/products/sizes';
import {Color} from '../../interfaces/color/color';
import {ToastrService} from 'ngx-toastr';
import {Router} from '@angular/router';
import {Collection} from '../../interfaces/products/collection';
import {Products} from "../../interfaces/products/products";
import {DeleteProducts} from "../../interfaces/products/delete-products";
import {NgbModal, NgbModalConfig} from "@ng-bootstrap/ng-bootstrap";
import {UpdateProducts} from "../../interfaces/products/update-products";


@Component({
  selector: 'app-products',
  templateUrl: './products.component.html',
  styleUrls: ['./products.component.scss']
})
export class ProductsComponent implements OnInit {
  public addProduct: FormGroup;
  public categories: Categories[];
  public color: Color[];
  public sizes: Sizes[] = [];
  public collection: Collection[] = [];
  public sub = [];
  public show = true;
  imageSrc: any;
  public index = 0;
  public disa = true;
  public event;
  public variants: any[] = [];
  public searchProducts: Products[] = [];
  public products: Products[] = [];
  public productDelete: Products;
  public productUpdate: Products;

  constructor(private Productservice: ProductsService, private toastr: ToastrService,
              private route: Router, private modalService: NgbModal, config: NgbModalConfig) {
    this.addProduct = new FormGroup({
      name: new FormControl('', [Validators.required, Validators.max(50)]),
      discount: new FormControl('', Validators.required),
      shortDetails: new FormControl('', [Validators.required, Validators.max(255)]),
      description: new FormControl('', Validators.required),
      stock: new FormControl('', Validators.required),
      new: new FormControl('', Validators.required),
      tela: new FormControl('', Validators.required),
      tecnica: new FormControl('', Validators.required),
      sale: new FormControl('', Validators.required),
      id_subcategory: new FormControl('', Validators.required),
      id_collection: new FormControl('', Validators.required),
      price: new FormControl('', Validators.required),
      sizes: new FormControl('', Validators.required),
      cat: new FormControl('', Validators.required)
    });
    config.backdrop = 'static';
  }

  ngOnInit() {
    this.getCat();
    this.getSize();
    this.getColor();
    this.getCollection();
    this.getProduct();
  }

  public change() {
    this.sub = this.categories[this.addProduct.value.cat - 1].sub_categories;
  }

  private getColor() {
    this.Productservice.getColor().subscribe((response: Color[]) => this.color = response);
  }

  private getProduct() {
    this.Productservice.getProduct().subscribe((response: Products[]) => {
      this.products = response;
      this.searchProducts = response;
    });
  }

  public getCollection() {
    this.Productservice.getCollection().subscribe((response: Collection[]) => this.collection = response);
  }

  private getCat() {
    this.Productservice.getCategories().subscribe((response: Categories[]) => this.categories = response);
  }

  private getSize() {
    this.Productservice.getSizes().subscribe((response: Sizes[]) => this.sizes = response);
  }

  public searchTerm(term: string, keys: string = 'shortDetails,name') {
    this.searchProducts = (this.products || []).filter((item) => keys.split(',').some(key => item.hasOwnProperty(key) && new RegExp(term, 'gi').test(item[key])));
  }

  public Product(form: FormGroup) {
    form.value.new = form.value.new !== '';
    form.value.sale = form.value.sale !== '';
    localStorage.setItem('product', JSON.stringify(form.value));
    this.show = false;
  }

  public visualizar(i: number, event) {
    if (event && event[i]) {
      const file = event[i];

      const reader = new FileReader();
      reader.onload = e => this.imageSrc = reader.result;

      reader.readAsDataURL(file);
    }
  }

  public changeImages(event) {
    this.event = event;
    this.variants = new Array(event.length);
    this.index = 0;
    this.visualizar(this.index, this.event);
  }

  public next(next = '') {
    if (next === 'true') {
      this.index++;
      if (this.event.length === this.index) {
        this.index = 0;
      }
    } else if (next === 'false') {
      this.index--;
      if (0 > this.index) {
        this.index = this.event.length - 1;
      }
    }
    this.visualizar(this.index, this.event);
  }

  public deleteProduct(id: number) {
    this.Productservice.deleteProduct(id).subscribe((response: DeleteProducts) => {
      this.toastr.success(response.message);
      this.products = response.products;
      this.searchProducts = response.products;
    }, error1 => {
      this.toastr.error(error1.error.message)
    })
  }

  public updateProduct(id: number, product) {
    this.Productservice.updateProduct(id, product.value).subscribe((response: UpdateProducts) => {
      this.addProduct.reset();
      delete this.productUpdate;
      this.toastr.success(response.message);
      this.products = response.products;
      this.searchProducts = response.products;
      this.modalService.dismissAll();
    }, error1 => {
      this.toastr.error(error1.error.message)
    })
  }

  open(content, product) {
    this.productDelete = product;
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
    }, (reason) => {
    });
  }

  open2(content, product) {
    this.productUpdate = product;
    this.addProduct.controls['cat'].setValue(this.productUpdate.sub_category.id_category);
    this.change();
    let price = '';
    let size = [];
    this.productUpdate.prices.forEach((e, i) => {
      if (i === this.productUpdate.prices.length - 1) {
        price += e.price;
      } else {
        price += e.price + ',';
      }
    });
    this.productUpdate.sizes.forEach(e => {
      size.push(e.sizes.id);
    });
    this.addProduct.controls['price'].setValue(price);
    this.addProduct.controls['name'].setValue(this.productUpdate.name);
    this.addProduct.controls['discount'].setValue(this.productUpdate.discount);
    this.addProduct.controls['shortDetails'].setValue(this.productUpdate.shortDetails);
    this.addProduct.controls['description'].setValue(this.productUpdate.description);
    this.addProduct.controls['stock'].setValue(this.productUpdate.stock);
    this.addProduct.controls['new'].setValue(this.productUpdate.new);
    this.addProduct.controls['tela'].setValue(this.productUpdate.tela);
    this.addProduct.controls['tecnica'].setValue(this.productUpdate.tecnica);
    this.addProduct.controls['sale'].setValue(this.productUpdate.sale);
    this.addProduct.controls['id_subcategory'].setValue(this.productUpdate.id_subcategory);
    this.addProduct.controls['id_collection'].setValue(this.productUpdate.id_collection);
    this.addProduct.controls['sizes'].setValue(size);
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title2', size: 'lg'}).result.then((result) => {
    }, (reason) => {
    });
  }

  public setColor(color) {
    this.variants[this.index] = color;
    this.toastr.success('Color agregado correctamente');
  }

  public addProductFinally(even) {
    even.preventDefault();
    if (this.event === undefined) {
      this.toastr.error('Por favor Seleccione al menos una imagen');
    } else {
      const tmp = [];
      for (let i = 0; i < this.variants.length; i++) {
        tmp.push(this.variants[i] === undefined ? null : this.variants[i]);
      }
      if (tmp.length <= 0) {
        this.toastr.error('Por favor selecciona al menos un color para una imagen');
      } else {
        this.Productservice.addProduct(JSON.parse(localStorage.getItem('product')), this.event, this.variants).subscribe(() => {
          this.toastr.success('Producto agregado correctamente');
          this.route.navigate(['admin']);
        }, error1 => {
          this.toastr.error(error1.error.message);
        });
      }
    }
  }

  public fileEvent($event) {
    const fileSelected: File = $event.target.files[0];
    this.Productservice.uploadFile(fileSelected)
      .subscribe((response) => {
          console.log(response);
        },
        error => {
          console.error(error);
        });
  }
}
