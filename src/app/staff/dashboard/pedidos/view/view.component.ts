import {Component, OnInit} from '@angular/core';
import {PedidosService} from '../../../services/pedidos/pedidos.service';
import {Pedidos} from '../../../interfaces/pedidos/pedidos';
import {Products} from '../../../interfaces/pedidos/products';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit {
  public orders: Pedidos[] = [];
  public searchOrders: Pedidos[] = [];

  constructor(private orderService: PedidosService) {
  }

  ngOnInit() {
    this.orderService.getOrders().subscribe((response: Pedidos[]) => {
      this.orders = response;
      this.orders.forEach(e => {
        e.estado = e.estado !== null ? e.estado : 'Cancelado';
        e.name = e.pago.buyerFullName;
      });
      this.searchOrders = this.orders;
    }, error1 => {
      console.error(error1);
    });
  }

  public greatTotal() {
    let t = 0;
    this.orders.forEach(e => {
      if (e.status === 'Confirmado' || e.status === 'En camino' || e.status === 'Entregado') {
        t += this.getTotal(e.products);
      }
    });
    return t;
  }

  public getTotal(mult: Products[]): number {
    return this.orderService.getTotalAmount(mult);
  }

  public searchTerm(term: string, keys: string = 'estado,created_at,name,entidad') {
    this.searchOrders = (this.orders || []).filter((item) => keys.split(',').some(key => item.hasOwnProperty(key) && new RegExp(term, 'gi').test(item[key])));
  }
}
