import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {Product} from '../../../../shared/classes/product';
import {CartItem} from '../../../../shared/classes/cart-item';
import {ProductsService} from '../../../../shared/services/products.service';
import {CartService} from '../../../../shared/services/cart.service';
import {Observable, of} from 'rxjs';

@Component({
  selector: 'app-quick-view',
  templateUrl: './quick-view.component.html',
  styleUrls: ['./quick-view.component.scss']
})
export class QuickViewComponent implements OnInit {

  public products: Product[] = [];
  public cartItems: Observable<CartItem[]> = of([]);
  public shoppingCartItems: CartItem[] = [];
  public counter: number = 1;
  public variantImage: any = '';
  public selectedColor: any = '';
  public selectedSize: any = '';

  constructor(private productsService: ProductsService, private toastrService: ToastrService,
              private cartService: CartService) {
  }

  ngOnInit() {
    this.productsService.getProducts().subscribe(product => this.products = product);
    this.cartItems = this.cartService.getItems();
    this.cartItems.subscribe(shoppingCartItems => {
      this.shoppingCartItems = shoppingCartItems;
    });
  }

  public increment() {
    this.counter += 1;
  }

  public decrement() {
    if (this.counter > 1) {
      this.counter -= 1;
    }
  }

  // Change variant images
  public changeVariantImage(image) {
    this.variantImage = image;
    this.selectedColor = image;
  }

  // Change variant
  public changeVariantSize(variant) {
    this.selectedSize = variant;
  }

  // If item is aleready added

  public addToCart(product: Product, quantity) {
    if (quantity === 0) {
      return false;
    }
    if (product.stock < quantity) {
      this.toastrService.error('No puede agregar más artículos que los disponibles. En stock ' + product.stock + ' artículos.'); // toasr services
    } else {
      if (this.selectedSize !== '') {
        this.cartService.addToCart(product, parseInt(quantity), this.selectedSize);
        this.toastrService.success('Este producto ha sido añadido al carrito.');
        this.selectedSize = '';
      } else {
        this.toastrService.error('Los artículos no se pueden agregar hasta que seleccione la talla'); // toasr services
      }
    }
  }

}
