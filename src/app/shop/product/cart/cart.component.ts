import {Component, OnInit} from '@angular/core';
import {ToastrService} from 'ngx-toastr';
import {CartItem} from '../../../shared/classes/cart-item';
import {ProductsService} from '../../../shared/services/products.service';
import {CartService} from '../../../shared/services/cart.service';
import {Observable, of} from 'rxjs';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.component.html',
  styleUrls: ['./cart.component.scss']
})
export class CartComponent implements OnInit {

  public cartItems: Observable<CartItem[]> = of([]);
  public shoppingCartItems: CartItem[] = [];


  constructor(private toastrService: ToastrService,
              public productsService: ProductsService, public cartService: CartService) {
  }

  ngOnInit() {
    this.cartItems = this.cartService.getItems();
    this.cartItems.subscribe(shoppingCartItems => {
      this.shoppingCartItems = shoppingCartItems;
    });
  }

  // Increase Product Quantity
  public increment(product: any, quantity: number = 1, size) {
    this.cartService.addToCart(product, quantity, size);
  }

  // Decrease Product Quantity
  public decrement(product: any, quantity = -1, size) {
    this.cartService.addToCart(product, quantity, size);
  }

  public movil(product: any, quantity, size) {
    this.cartService.addToCart(product, quantity, size, true);
  }

  // Remove cart items
  public removeItem(item: CartItem) {
    this.cartService.removeFromCart(item);
  }

}
