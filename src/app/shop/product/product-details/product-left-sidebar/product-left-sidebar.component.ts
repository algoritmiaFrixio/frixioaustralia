import {Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {ToastrService} from 'ngx-toastr';
import {Product} from '../../../../shared/classes/product';
import {CartItem} from '../../../../shared/classes/cart-item';
import {ProductsService} from '../../../../shared/services/products.service';
import {WishlistService} from '../../../../shared/services/wishlist.service';
import {CartService} from '../../../../shared/services/cart.service';
import {Observable, of} from 'rxjs';
import * as $ from 'jquery';

@Component({
  selector: 'app-product-left-sidebar',
  templateUrl: './product-left-sidebar.component.html',
  styleUrls: ['./product-left-sidebar.component.scss']
})
export class ProductLeftSidebarComponent implements OnInit {

  public product: Product = {};
  public products: Product[] = [];
  public cartItems: Observable<CartItem[]> = of([]);
  public shoppingCartItems: CartItem[] = [];
  public counter = 1;
  public image = '';
  public selectedSize = '';
  public descrip1;
  public descrip2;
  public slideConfig = {
    slidesToShow: 1,
    slidesToScroll: 1,
    arrows: true,
    fade: true,
  };
  public slideNavConfig = {
    vertical: false,
    slidesToShow: 3,
    slidesToScroll: 1,
    asNavFor: '.product-slick',
    arrows: false,
    dots: false,
    focusOnSelect: true
  };

  //Get Product By Id
  constructor(private route: ActivatedRoute, private router: Router, private toastrService: ToastrService,
              public productsService: ProductsService, private wishlistService: WishlistService,
              public cartService: CartService) {
    this.route.params.subscribe(params => {
      const id = params['id'];
      this.productsService.getProduct(id).subscribe(product => {
        this.product = product;
        this.image = this.product.pictures[0];
        if (id === 'Pago-de-afiliación') {
          this.selectedSize = this.product.size[0];
        }
        this.descrip1 = product.description.split('.')[0] + '.';
        this.descrip2 = product.description.split('.')[1] + '.';
      });
    });
  }

  ngOnInit() {
    this.productsService.getProducts().subscribe(product => this.products = product);
    this.cartItems = this.cartService.getItems();
    this.cartItems.subscribe(shoppingCartItems => {
      this.shoppingCartItems = shoppingCartItems;
    });
  }

  // product zoom
  onMouseOver(): void {
    document.getElementById('p-zoom').classList.add('product-zoom');
  }

  changeImg(id) {
    this.image = this.product.pictures[id];
  }

  onMouseOut(): void {
    document.getElementById('p-zoom').classList.remove('product-zoom');
  }

  public increment() {
    this.counter += 1;
  }

  public decrement() {
    if (this.counter > 1) {
      this.counter -= 1;
    }
  }

  // For mobile filter view
  public mobileSidebar() {
    $('.collection-filter').css('left', '-15px');
  }

  // If item is aleready added      console.log(data);

  public hasItem(product: Product): boolean {
    const item = this.shoppingCartItems.find(item => item.product.id === product.id);
    return item !== undefined;
  }

  // Add to cart
  public addToCart(product: Product, quantity) {
    if (quantity === 0) {
      return false;
    }
    if (product.stock < quantity) {
      this.toastrService.error('No puede agregar más artículos que los disponibles. En stock ' + product.stock + ' artículos.'); // toasr services
    } else {
      if (this.selectedSize !== '') {
        this.cartService.addToCart(product, parseInt(quantity), this.selectedSize);
        this.toastrService.success('Este producto ha sido añadido al carrito.');
        this.selectedSize = '';
      } else {
        this.toastrService.error('Los artículos no se pueden agregar hasta que seleccione la talla'); // toasr services
      }
    }
  }

  // Add to cart
  public buyNow(product: Product, quantity) {
    if (quantity > 0) {
      if (this.selectedSize !== '') {
        this.cartService.addToCart(product, parseInt(quantity), this.selectedSize);
        this.selectedSize = '';
        this.router.navigate(['/inicio/comprar']);
      } else {
        this.toastrService.error('Los artículos no se pueden agregar hasta que seleccione la talla'); // toasr services
      }
    }
  }

  // Add to wishlist
  public addToWishlist(product: Product) {
    this.wishlistService.addToWishlist(product);
  }


  // Change size variant
  public changeSizeVariant(variant) {
    this.selectedSize = variant;
  }

}
