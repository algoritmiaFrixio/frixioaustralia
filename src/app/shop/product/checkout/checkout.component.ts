import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {CartItem} from '../../../shared/classes/cart-item';
import {ProductsService} from '../../../shared/services/products.service';
import {CartService} from '../../../shared/services/cart.service';
import {OrderService} from '../../../shared/services/order.service';
import {Observable, of} from 'rxjs';
import {Login} from '../../../shared/classes/login/login';
import {NgbModal, NgbModalConfig} from "@ng-bootstrap/ng-bootstrap";
import {Router} from "@angular/router";

@Component({
  selector: 'app-checkout',
  templateUrl: './checkout.component.html',
  styleUrls: ['./checkout.component.scss']
})
export class CheckoutComponent implements OnInit {
  public banco: string;
  public img: string;
  public desc: string;
  public sec: number = 5;
  public id;
  // form group
  public checkoutForm: FormGroup;
  public passwordForm: FormGroup;
  public paises;
  public cartItems: Observable<CartItem[]> = of([]);
  public checkOutItems: CartItem[] = [];
  public amount: number;
  public cuenta = false;
  public pass = true;
  public pass2 = true;
  public datos: Login;
  public porcTotal = 0.81;
  public porcTax = 0.19;
  public dtco = 0;
  public porcShipping = 0;

  // Form Validator
  constructor(private fb: FormBuilder, private fb2: FormBuilder, private cartService: CartService,
              public productsService: ProductsService, private orderService: OrderService,
              private modalService: NgbModal, config: NgbModalConfig, private router: Router) {
    config.backdrop = 'static';
    this.datos = {
      frixionista: false
    };
    this.checkoutForm = this.fb.group({
      firstname: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z ]$')]],
      lastname: ['', [Validators.required, Validators.pattern('[a-zA-Z][a-zA-Z ]+[a-zA-Z ]$')]],
      telephone: ['', [Validators.required, Validators.pattern('[0-9 ]+')]],
      buyerEmail: ['', [Validators.required, Validators.email]],
      shippingAddress: ['', [Validators.required, Validators.maxLength(50)]],
      shippingCountry: ['', Validators.required],
      cedula: ['', Validators.required],
      shippingCity: ['', Validators.required],
      state: ['', Validators.required],
    });
    this.passwordForm = this.fb2.group(({
      contrasena: ['', Validators.required],
    }));
  }

  ngOnInit() {
    if (localStorage.getItem(btoa('auth')) !== null) {
      this.datos = JSON.parse(atob(localStorage.getItem(btoa('auth'))));
      this.cuenta = false;
      const tmp = this.datos.cliente.buyerFullName.split(' ');
      let firstname = '';
      let lastname = '';
      switch (tmp.length) {
        case 2:
          firstname += tmp[1];
          lastname += tmp[0];
          break;
        case 3:
          firstname += tmp[2];
          lastname += tmp[0] + ' ';
          lastname += tmp[1];
          break;
        case 4:
          firstname += tmp[2] + ' ';
          firstname += tmp[3];
          lastname += tmp[0] + ' ';
          lastname += tmp[1];
          break;
        case 5:
          firstname += tmp[2] + ' ';
          firstname += tmp[3] + ' ';
          firstname += tmp[4];
          lastname += tmp[0] + ' ';
          lastname += tmp[1];
      }
      this.checkoutForm.controls['firstname'].setValue(firstname);
      this.checkoutForm.controls['lastname'].setValue(lastname);
      this.checkoutForm.controls['telephone'].setValue(this.datos.cliente.telephone);
      this.checkoutForm.controls['buyerEmail'].setValue(this.datos.cliente.buyerEmail);
      this.checkoutForm.controls['shippingAddress'].setValue(this.datos.cliente.shippingAddress);
      this.checkoutForm.controls['shippingCountry'].setValue(this.datos.cliente.shippingCountry);
      this.checkoutForm.controls['shippingCity'].setValue(this.datos.cliente.shippingCity);
      this.checkoutForm.controls['state'].setValue(this.datos.cliente.state);
      this.checkoutForm.controls['cedula'].setValue(this.datos.cliente.identification);
    }
    this.orderService.getCountries().subscribe(response => {
      this.paises = response;
    });
    this.cartItems = this.cartService.getItems();
    this.cartItems.subscribe(products => {
      this.checkOutItems = products;
      let quant = 0;
      products.forEach(e => {
        quant += e.quantity;
      });
      // if (this.datos.frixionista) {
      if (quant < 4) {
        this.porcShipping = 6000;
        // }
      }
      this.amount = this.getTotal(1);
      this.dtco = this.getTotals(1) * this.cartService.porcdtco;
    });
    this.checkoutForm.valueChanges.subscribe(() => {
      this.pass = (!this.checkoutForm.valid) || !this.passwordForm.valid;
      this.pass2 = !this.checkoutForm.valid;
    });
    this.passwordForm.valueChanges.subscribe(() => {
      this.pass = (!this.checkoutForm.valid) || !this.passwordForm.valid;
    });
  }


  // Get sub Total
  public getTotal(mult) {
    return this.cartService.getTotal(mult);
  }

  public getTotalimpuestos(mult) {
    return this.cartService.getTotal(mult);
  }

  public getTotals(mult) {
    return this.cartService.getTotals(mult);
  }

  // stripe payment gateway
  stripeCheckout(form: FormGroup, pass: FormGroup, type: boolean, efectivo: boolean = false, data: DatosEfectivo = {}, trans = false) {
    form.value.shippingValue = this.porcShipping;
    form.value.products = '';
    form.value.size = '';
    form.value.quantity = '';
    this.cartService.getItems().subscribe((r: CartItem[]) => {
      r.forEach((e, i) => {
        if (i === r.length - 1) {
          form.value.products += e.product.id;
          form.value.size += e.size;
          form.value.quantity += e.quantity;
        } else {
          form.value.products += e.product.id + ',';
          form.value.size += e.size + ',';
          form.value.quantity += e.quantity + ',';
        }
      });
    });
    form.value.buyerFullName = form.value.firstname + ' ' + form.value.lastname;
    form.value.amount = this.amount + this.porcShipping;
    form.value.contrasena = pass.value.contrasena !== '' ? pass.value.contrasena : null;
    form.value.tax = form.value.amount * 0.19;
    form.value.dtco = this.dtco;
    form.value.currency = 'AUD';
    form.value.taxReturnBase = form.value.amount - form.value.tax;
    form.value.description = 'Compra de frixio productos bordados a mano';
    const client: Login = localStorage.getItem(btoa('auth')) !== null ? JSON.parse(atob(localStorage.getItem(btoa('auth')))) : null;
    form.value.id_cliente = client !== null ? client.cliente.id : null;
    this.orderService.payu(form.value).subscribe(response => {
      this.orderService.createOrder(this.checkOutItems, this.checkoutForm.value,
        response.id, form.value.amount, form.value.shippingValue, form.value.tax, type, this.dtco);
      this.id = response.id;
      if (efectivo) {
        const tmp = btoa(JSON.stringify(data));
        this.orderService.cash(tmp, form.value.buyerFullName, this.id, form.value.buyerEmail);
        location.href = `/inicio/comprar/estado/7/${this.id}/${this.banco}/Efectivo`;
      }
      if (trans) {
        const tmp = btoa(JSON.stringify(data));
        this.orderService.trans(tmp, form.value.buyerFullName, this.id, form.value.buyerEmail).subscribe(res => console.log(res));
      }
    });
  }

  open(content, banco, img, form: FormGroup, pass: FormGroup) {
    this.sec = 5;
    this.img = img;
    this.banco = banco;
    let url = "";
    switch (this.banco) {
      case 'Colpatria':
        this.desc = "Cuenta corriente  N° 8851000950 Carlos Alberto Perea Cardona ";
        url = 'https://bit.ly/2Derr2P';
        break;
      case 'Bancolombia':
        this.desc = "Cuenta de ahorros N° 72812003471 Carlos Alberto Perea Cardona ";
        url = "https://bit.ly/2SbOqpE";
        break;
      case 'Davivienda':
        this.desc = "Cuenta de ahorros N° 0000144557402 Carlos Alberto Perea Cardona ";
        url = "https://bit.ly/2fzb7Rv";
        break;
    }
    let interval = setInterval(() => {
      this.sec--;
      if (this.sec === 0) {
        clearInterval(interval);
        let data: DatosEfectivo = {};
        data.tipo = banco;
        data.imagen = img;
        data.cedula = form.value.cedula;
        data.descuento = this.dtco;
        data.total = this.amount + this.porcShipping;
        data.envio = this.porcShipping;
        data.products = [];
        this.checkOutItems.forEach((e, i) => {
          data.products[i] = {
            ref: e.product.ref,
            quantity: e.quantity,
            precio: e.product.price - (e.product.price * this.cartService.porcdtco)
          }
        });
        window.open(url);
        this.stripeCheckout(form, pass, false, false, data, true);

      }
    }, 1000);
    this.modalService.open(content, {ariaLabelledBy: 'modal-basic-title'}).result.then((result) => {
    }, (reason) => {

    });
  }

  public cash(tipo: string, img, form: FormGroup, pass: FormGroup) {
    this.banco = tipo;
    let data: DatosEfectivo = {};
    data.tipo = tipo;
    data.imagen = img;
    data.cedula = form.value.cedula;
    data.descuento = this.dtco;
    data.total = this.amount + this.porcShipping;
    data.envio = this.porcShipping;
    data.products = [];
    this.checkOutItems.forEach((e, i) => {
      data.products[i] = {
        ref: e.product.ref,
        quantity: e.quantity,
        precio: e.product.price - (e.product.price * this.cartService.porcdtco)
      }
    });
    this.stripeCheckout(form, pass, false, true, data);
  }

  public close() {
    location.href = `/inicio/comprar/estado/7/${this.id}/${this.banco}/Transferencia bancaria`;
  }

}

interface DatosEfectivo {
  tipo?: string;
  cedula?: string;
  imagen?: string;
  total?: number;
  descuento?: number;
  envio?: number;
  products?: products[]
}

interface products {
  ref?: string;
  quantity?: number;
  precio?: number;
}
