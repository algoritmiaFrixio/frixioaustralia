import {ChangeDetectionStrategy, Component, OnInit} from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {animate, style, transition, trigger} from '@angular/animations';
import {ColorFilter, Product} from '../../../../shared/classes/product';
import {ProductsService} from '../../../../shared/services/products.service';
import * as $ from 'jquery';
import * as _ from 'lodash';
import {CartService} from "../../../../shared/services/cart.service";


@Component({
  selector: 'app-collection-left-sidebar',
  templateUrl: './collection-left-sidebar.component.html',
  styleUrls: ['./collection-left-sidebar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [  // angular animation
    trigger('Animation', [
      transition('* => fadeOut', [
        style({opacity: 0.1}),
        animate(1000, style({opacity: 0.1}))
      ]),
      transition('* => fadeIn', [
        style({opacity: 0.1}),
        animate(1000, style({opacity: 0.1}))
      ])
    ])
  ]
})
export class CollectionLeftSidebarComponent implements OnInit {

  public products: Product[] = [];
  public items: Product[] = [];
  public allItems: Product[] = [];
  public colorFilters: ColorFilter[] = [];
  public tagsFilters: any[] = [];
  public tags: any[] = [];
  public colors: any[] = [];
  public sortByOrder: string = '';   // sorting
  public animation: any;   // Animation
  public price: number[] = [];

  lastKey = '';      // key to offset next query from
  finished = false;

  // boolean when end of data is reached

  constructor(private route: ActivatedRoute, private router: Router,
              private productsService: ProductsService, private cartService: CartService) {

    this.route.params.subscribe(params => {
      const category = params['category'];
      this.productsService.getProductByCategory(category).subscribe((products: Product[]) => {
        this.allItems = products;  // all products
        this.items = products;  // all products
        this.products = products.slice(0, 8);
        products.forEach(e => {
          this.price.push(parseInt((e.price - (e.price * this.cartService.porcdtco)).toString()));
        });
        this.getTags(products);
        this.getColors(products);
      }, error1 => console.error(error1));
    });
  }

  ngOnInit() {
  }

  // Get current product tags
  public getTags(products) {
    const uniqueBrands = [];
    const itemBrand = Array();
    products.map((product, index) => {
      if (product.tags) {
        product.tags.map((tag) => {
          index = uniqueBrands.indexOf(tag);
          if (index === -1) uniqueBrands.push(tag);
        });
      }
    });
    for (let i = 0; i < uniqueBrands.length; i++) {
      itemBrand.push({brand: uniqueBrands[i]});
    }
    this.tags = itemBrand;
  }

  // Get current product colors
  public getColors(products) {
    const uniqueColors = [];
    const itemColor = Array();
    products.map((product, index) => {
      if (product.colors) {
        product.colors.map((color) => {
          const index = uniqueColors.indexOf(color);
          if (index === -1) {
            uniqueColors.push(color);
          }
        });
      }
    });
    for (let i = 0; i < uniqueColors.length; i++) {
      itemColor.push({color: uniqueColors[i]});
    }
    this.colors = itemColor;
  }


  // Animation Effect fadeIn
  public fadeIn() {
    this.animation = 'fadeIn';
  }

  // Animation Effect fadeOut
  public fadeOut() {
    this.animation = 'fadeOut';
  }

  // Initialize filetr Items
  public filterItems(): Product[] {
    return this.products.filter((item: Product) => {
      return this.colorFilters.reduce((prev, curr) => { // Match Color
        if (item.colors) {
          if (item.colors.includes(curr.color)) {
            return prev && true;
          }
        }
      }, true); // return true
    });
  }

  // Update tags filter
  public updateTagFilters(tags: any[]) {
    this.tagsFilters = tags;
    this.animation == 'fadeOut' ? this.fadeIn() : this.fadeOut(); // animation
  }

  // Update color filter
  public updateColorFilters(colors: any[]) {
    this.colorFilters = colors;
    this.animation == 'fadeOut' ? this.fadeIn() : this.fadeOut(); // animation
  }

  // Update price filter
  public updatePriceFilters(price: any) {
    let items: any[] = [];
    this.products.filter((item: Product) => {
      if ((item.price - (item.price * this.cartService.porcdtco)) >= price[0] && (item.price - (item.price * this.cartService.porcdtco)) <= price[1]) {
        items.push(item); // push in array
      }
    });
    this.products = items;
  }

  public twoCol() {
    if ($('.product-wrapper-grid').hasClass('list-view')) {
    } else {
      $('.product-wrapper-grid').children().children().children().removeClass();
      $('.product-wrapper-grid').children().children().children().addClass('col-lg-6');
    }
  }

  public threeCol() {
    if ($('.product-wrapper-grid').hasClass('list-view')) {
    } else {
      $('.product-wrapper-grid').children().children().children().removeClass();
      $('.product-wrapper-grid').children().children().children().addClass('col-lg-4');
    }
  }

  public fourCol() {
    if ($('.product-wrapper-grid').hasClass('list-view')) {
    } else {
      $('.product-wrapper-grid').children().children().children().removeClass();
      $('.product-wrapper-grid').children().children().children().addClass('col-lg-3');
    }
  }

  public sixCol() {
    if ($('.product-wrapper-grid').hasClass('list-view')) {
    } else {
      $('.product-wrapper-grid').children().children().children().removeClass();
      $('.product-wrapper-grid').children().children().children().addClass('col-lg-2');
    }
  }

  // For mobile filter view
  public mobileFilter() {
    $('.collection-filter').css('left', '-15px');
  }


  public onScroll() {
    this.lastKey = _.last(this.allItems).id;
    if (this.lastKey !== _.last(this.items).id) {
      this.finished = false;
    }
    // If data is identical, stop making queries
    if (this.lastKey === _.last(this.items).id) {
      this.finished = true;
    }
    if (this.products.length < this.allItems.length) {
      const len = this.products.length;
      for (let i = len; i < len + 4; i++) {
        if (this.allItems[i] === undefined) {
          return true;
        }
        this.products.push(this.allItems[i]);
      }
    }
  }

  // sorting type ASC / DESC / A-Z / Z-A etc.
  public onChangeSorting(val) {
    this.sortByOrder = val;
    this.animation === 'fadeOut' ? this.fadeIn() : this.fadeOut(); // animation
  }

}
