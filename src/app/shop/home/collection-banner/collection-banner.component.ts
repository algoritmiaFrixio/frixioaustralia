import {Component, OnInit} from '@angular/core';

@Component({
  selector: 'app-collection-banner',
  templateUrl: './collection-banner.component.html',
  styleUrls: ['./collection-banner.component.scss']
})
export class CollectionBannerComponent implements OnInit {

  constructor() {
  }

  ngOnInit() {
  }

  // Collection banner
  public category = [{
    image: 'assets/images/sub-banner1.png',
    save: 'Art Dress Up',
    title: 'Man',
    link: '/inicio/categoria/hombre'
  }, {
    image: 'assets/images/sub-banner2.png',
    save: 'Art Dress Up',
    title: 'Man',
    link: '/inicio/categoria/hombre'
  }];

}
