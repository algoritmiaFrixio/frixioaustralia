import { Component, OnInit } from '@angular/core';
import {LoginService} from "../../../shared/services/login.service";

@Component({
  selector: 'app-services',
  templateUrl: './services.component.html',
  styleUrls: ['./services.component.scss']
})
export class ServicesComponent implements OnInit {

  constructor(public LoginService: LoginService) { }

  ngOnInit() { }
  
}
