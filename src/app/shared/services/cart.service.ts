import {Injectable} from '@angular/core';
import {Product} from '../classes/product';
import {CartItem} from '../classes/cart-item';
import {ToastrService} from 'ngx-toastr';
import {BehaviorSubject, Observable, Subscriber} from 'rxjs';

@Injectable({
  providedIn: 'root'
})

export class CartService {

  // Array
  public cartItems: BehaviorSubject<CartItem[]> = new BehaviorSubject([]);
  public observer: Subscriber<{}>;
  public product: CartItem[] = [];
  public porcdtco: number = 0;

  constructor(private toastrService: ToastrService) {
    this.cartItems.subscribe(products => this.product = products);
    this.cartItems.next(localStorage.getItem(btoa('cart')) === null ? [] : JSON.parse(atob(localStorage.getItem(btoa('cart')))));
  }

  // Get Products
  public getItems(): Observable<CartItem[]> {
    const itemsStream = new Observable(observer => {
      observer.next(this.product);
      observer.complete();
    });
    return <Observable<CartItem[]>>itemsStream;
  }

  // If item is aleready added
  public hasItem(product: Product): boolean {
    const item = this.product.find(item => item.product.id === product.id);
    return item !== undefined;
  }

  // Add to cart
  public addToCart(product: Product, quantity: number, size: string = '', movil: boolean = false): CartItem | boolean {
    const item: CartItem | boolean = false;
    if (quantity === 0) {
      return false;
    }
    if (this.hasItem(product)) {
      const item = this.product.filter(item => item.product.id === product.id)[0];
      const index = this.product.indexOf(item);
      let qty = 0;
      if (movil) {
        qty = quantity;
      } else {
        qty = this.product[index].quantity + quantity;
      }
      const stock = this.product[index].product.stock;
      if (this.product[index].size === size) {
        if (stock < qty) {
          return false;
        } else {
          this.product[index].quantity = qty;
        }
      } else {
        if (product.stock < quantity) {
          return false;
        } else {
          const item = {
            product: product,
            quantity: quantity,
            size: size
          };
          this.product.push(item);
        }
      }
    } else {
      if (product.stock < quantity) {
        return false;
      } else {
        const item = {
          product: product,
          quantity: quantity,
          size: size
        };
        this.product.push(item);
      }
    }
    localStorage.setItem(btoa('cart'), btoa(JSON.stringify(this.product)));
    this.cartItems.next(this.product);
    return item;
  }


  // Removed in cart
  public removeFromCart(item: CartItem) {
    if (item === undefined) {
      return false;
    }
    const index = this.product.indexOf(item);
    this.product.splice(index, 1);
    localStorage.setItem(btoa('cart'), btoa(JSON.stringify(this.product)));
  }


  public getTotal(mult) {
    let total = 0;
    this.cartItems.forEach((product) => {
      product.forEach(e => {
        total += e.product.price * e.quantity;
      });
    });
    return (total - (total * this.porcdtco)) * mult;
  }

  public getTotals(mult) {
    let total = 0;
    this.cartItems.forEach((product) => {
      product.forEach(e => {
        total += e.product.price * e.quantity;
      });
    });
    return (total) * mult;
  }
}
