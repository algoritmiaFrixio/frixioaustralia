import {Component, OnInit} from '@angular/core';
import {NavbarService} from '../../../services/navbar.service';
import {Categories} from '../../../classes/categories';
import {ToastrService} from 'ngx-toastr';
import * as $ from 'jquery';
@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent implements OnInit {

  public categories: Categories[];
  public SubCategories: any[] = [];

  constructor(private _NavbarService: NavbarService, private _toastr: ToastrService) {
  }

  ngOnInit() {
    this.getCategories();
  }

  public getCategories() {
    this._NavbarService.getCategories().subscribe((response: Categories[]) => {
        this.categories = response;
        this.categories.forEach(e => {
          this.SubCategories.push(e.sub_categories);
        });
      },
      error1 => {
        console.error(error1);
        this._toastr.error(error1.error.message);
      });
  }

  public hideMenu(){
    $('.sm-horizontal').css("right", "-410px");
  }
}
