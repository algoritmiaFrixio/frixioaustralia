export interface Cliente {
  buyerEmail?: string;
  buyerFullName?: string;
  id?: number;
  identification?: string;
  postalcode?: string;
  shippingAddress?: string;
  shippingCity?: string;
  shippingCountry?: string;
  state?: string;
  telephone?: string;
}
